package com.testing.coupon.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.testing.coupon.model.Coupon;

public interface CouponRepo extends JpaRepository<Coupon, Long> {

	@Query(nativeQuery = true, value = "select * from coupon c where c.expire_date<? and deleted=?")
	List<Coupon> findAllByExpiration(String date, Boolean deleted);

	Optional<Coupon> findByCoupon(String code);

	@Query(nativeQuery = true, value = "select redeem_by from coupon")
	List<Coupon> redeemUserList();
}
