package com.testing.coupon.repository;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.testing.coupon.model.Coupon;

@Repository
public interface CouponRepository {
	String generate(Coupon coupon);
	List<Coupon> getCouponList(boolean deleted) ;
	List<Coupon> deleteCoupon(Long id);
}
