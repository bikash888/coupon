package com.testing.coupon.repository;

import java.security.SecureRandom;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.testing.coupon.model.Coupon;

@Service
public class CouponService implements CouponRepository {

	@Autowired
	private CouponRepo couponRepo;

	@Override
	public String generate(Coupon coupon) {
		String generateCoupon = getCoupon(coupon.getCouponType());
		coupon.setCoupon(generateCoupon);
		coupon.setCreatedAt(new Date());
		couponRepo.save(coupon);
		return coupon.getCoupon();
	}

	private String getCoupon(String cuponType) {
		int codeLength = 5;
		char[] chars = "abcdefghijklmnopqrstuvwxyz1234567890".toCharArray();
		StringBuilder sb = new StringBuilder();
		Random random = new SecureRandom();
		for (int i = 0; i < codeLength; i++) {
			char c = chars[random.nextInt(chars.length)];
			sb.append(c);
		}
		return "PREPG-" + sb.toString().toUpperCase();
	}

	@Override
	public List<Coupon> getCouponList(boolean deleted) {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		String date = format.format(new Date());
		System.out.println(date);
		return couponRepo.findAllByExpiration(date, deleted);
	}

	@Override
	public List<Coupon> deleteCoupon(Long id) {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		String date = format.format(new Date());
		Coupon coupon = couponRepo.findById(id).get();
		coupon.setDeletedAt(new Date());
		coupon.setDeletedBy("A");
		couponRepo.save(coupon);
		return couponRepo.findAllByExpiration(date, false);
	}
	
	
	

}
