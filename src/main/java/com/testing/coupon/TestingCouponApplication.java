package com.testing.coupon;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestingCouponApplication {

	public static void main(String[] args) {
		SpringApplication.run(TestingCouponApplication.class, args);
	}

}
