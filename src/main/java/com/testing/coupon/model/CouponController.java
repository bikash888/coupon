package com.testing.coupon.model;

import java.security.Principal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.testing.coupon.repository.CouponRepo;
import com.testing.coupon.repository.CouponService;

@RestController
public class CouponController {

	@Autowired
	private CouponService couponServices;

	@Autowired
	private CouponRepo repo;

	SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
	String date = format.format(new Date());

	@PostMapping("/coupon")
	public ResponseEntity<?> generateCoupon(Principal principal, @RequestBody Coupon coupon) {
		return new ResponseEntity<>(couponServices.generate(coupon), HttpStatus.OK);
	}

	@GetMapping("/coupon/{deleted}")
	public ResponseEntity<?> getCoupon(@PathVariable boolean deleted) {
		return new ResponseEntity<>(couponServices.getCouponList(deleted), HttpStatus.OK);
	}

	@PutMapping("addUser/{code}/{user}")
	public ResponseEntity<String> addReedemUsers(@PathVariable String code, @PathVariable String user) {
		Coupon redeemCoupon = repo.findByCoupon(code).get();
		String expiredDate = redeemCoupon.getExpireDate().toString();
		if (date.compareTo(expiredDate) <= 0 && redeemCoupon.getDeleted().equals(false)) {
			List<String> redeemBy = Arrays.asList(redeemCoupon.getRedeem_by().split(","));
			if (!redeemBy.contains(user)) {
				if (redeemBy.size() < redeemCoupon.getMax_redemptions()) {
					redeemCoupon.setRedeem_by(redeemCoupon.getRedeem_by() + "," + user);
					redeemCoupon.setId(redeemCoupon.getId());
					repo.save(redeemCoupon);
				} else {
					return new ResponseEntity<>("User Limit exceeds!!!", HttpStatus.OK);
				}
			}
			else {
				return new ResponseEntity<>(user+" already exist",HttpStatus.OK);
			}
		} else {

			return new ResponseEntity<>("Code is either deleted or expired", HttpStatus.OK);

		}

		return ResponseEntity.ok("new user added");

	}

	/*
	 * case of coupon expired >>deleted-true >>max-redepmtions ==redeem user
	 * >>coupon expired
	 */
	@GetMapping("/coupon/expired")
	public List<String> findExpiredCouponFromAllPossibleCase() {
		List<Coupon> findAllCoupon = repo.findAll();
		List<String> expiredCouponCollection = new ArrayList<String>();
		for (Coupon coupons : findAllCoupon) {
			String[] redeemList = coupons.getCoupon().split(",");
			System.out.println(redeemList.length);
			if (coupons.getMax_redemptions() == redeemList.length || coupons.getDeleted().equals(true)
					|| date.compareTo(coupons.getExpireDate().toString()) <= 0) {
				expiredCouponCollection.add(coupons.getCoupon());
			}

		}
		return expiredCouponCollection;
	}

	@DeleteMapping("/coupon/{id}")
	public ResponseEntity<String> deleteCoupon(Principal principal, @PathVariable Long id) {
		couponServices.deleteCoupon(id);
		return ResponseEntity.ok("deleted");
	}

	@GetMapping("/all")
	public List<Coupon> findAllCouponsDetails() {
		return repo.findAll();
	}

	// finding list of redeemUsers

	@GetMapping("/findRedeemUsers/{coupon}")
	public List<String> findRedeemUserList(@PathVariable String coupon) {
		Optional<Coupon> findUsersLists = repo.findByCoupon(coupon);
		List<String> redeemUserList = Arrays.asList(findUsersLists.get().getRedeem_by().split(","));
		return redeemUserList;

	}

}
