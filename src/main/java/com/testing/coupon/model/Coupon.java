package com.testing.coupon.model;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.sun.istack.NotNull;

import lombok.Builder;
import lombok.Data;


@Data
@Entity
@Table(name="coupon")
public class Coupon implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 6833427919204858913L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")
	private Long id;

	private String coupon;
	
	@Temporal(TemporalType.DATE)
	private Date expireDate;

	@NotNull
	private Integer max_redemptions; // mass,number
	@NotNull
	private String targetSources;// test,plan,A,B
	@NotNull
	private String couponType;// A,P,F

	private Double percent_off;

	private Date createdAt;
	@NotNull
	private String createdBy;
	
	@Column(name = "deleted",columnDefinition="tinyint(1) default 0")
	private Boolean deleted;
	
	private String deletedBy;

	@Temporal(TemporalType.DATE)
	private Date deletedAt;
	
	private String redeem_by; //a,b,c
	
	private String name;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCoupon() {
		return coupon;
	}

	public void setCoupon(String coupon) {
		this.coupon = coupon;
	}

	public Date getExpireDate() {
		return expireDate;
	}

	public void setExpireDate(Date expireDate) {
		this.expireDate = expireDate;
	}

	public Integer getMax_redemptions() {
		return max_redemptions;
	}

	public void setMax_redemptions(Integer max_redemptions) {
		this.max_redemptions = max_redemptions;
	}

	public String getTargetSources() {
		return targetSources;
	}

	public void setTargetSources(String targetSources) {
		this.targetSources = targetSources;
	}

	public String getCouponType() {
		return couponType;
	}

	public void setCouponType(String couponType) {
		this.couponType = couponType;
	}

	public Double getPercent_off() {
		return percent_off;
	}

	public void setPercent_off(Double percent_off) {
		this.percent_off = percent_off;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Boolean getDeleted() {
		return deleted;
	}

	public void setDeleted(Boolean deleted) {
		this.deleted = deleted;
	}

	public String getDeletedBy() {
		return deletedBy;
	}

	public void setDeletedBy(String deletedBy) {
		this.deletedBy = deletedBy;
	}

	public Date getDeletedAt() {
		return deletedAt;
	}

	public void setDeletedAt(Date deletedAt) {
		this.deletedAt = deletedAt;
	}

	public String getRedeem_by() {
		return redeem_by;
	}

	public void setRedeem_by(String redeem_by) {
		this.redeem_by = redeem_by;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	


	
	


	
}
